# README #

rmsiGUI is a graphical user interface for analyzing mass spectrometry imaging (MSI) data with statistical language R. 

### Status ###

Early development. For productive work, currently the command line version MSI.R is recommended: https://bitbucket.org/lababi/msi.r/.

### Installation ###

You have to install the libraries "devtools" and "shiny", as well as Sebastian Gibb's rmsi library: https://bitbucket.org/sgibb/rmsi/.

### Contribution guidelines ###

Please report your experiences, feature requests and problems.

### License ###

GNU General Public License, version 3 (http://gplv3.fsf.org/).

### Contact ###

robert.winkler@ira.cinvestav.mx, robert.winkler@bioprocess.org